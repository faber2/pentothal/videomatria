#!/bin/bash

LC_COLLATE="C"
HOST="k9.tzertu.net"
PORT="8090"
USR="source"
PASS="hackme"
MOUNT="test.ogg"


while [ -n "$1" ]; do # while loop starts

	case "$1" in

	-h) $HOST=$1 ;;
    -p) $PASS=$1 ;;
    -P) $PORT=$1 ;;
    -u) $USR=$1 ;;
    -m) $MOUNT=$1 ;;

	--)
		shift # The double dash which separates options from parameters

		break
		;; # Exit the loop using break command

	*) echo "Option $1 not recognized" ;;

	esac

	shift

done


ffmpeg \
  -f v4l2 -video_size 640x480 -framerate 30 -i /dev/video0 \
  -f pulse -i default \
  -f webm -cluster_size_limit 2M -cluster_time_limit 5100 -content_type video/webm \
  -c:a libvorbis -b:a 96K \
  -c:v libvpx -b:v 1.5M -crf 30 -g 150 -deadline good -threads 4 \
  icecast://$USR:$PASS@$HOST:$PORT/$MOUNT
